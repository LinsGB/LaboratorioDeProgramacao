class FileUtils:

    def readFile(self, file_name):
        file = open(file_name, "r+")

        lines = file.readlines()

        file.close()

        return lines

    def writeToFile(self, text, file_name):
        file = open(file_name, "a+")

        file.write(str(text)+"\n")

        file.close()